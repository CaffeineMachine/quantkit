# from pprint import pprint as pp
from pyutils.structures.args import *
from pyutils.utilities.sequtils import Ind
from pyutils.output.testing import *
from pyutils.output.forms import *

from quantkit.assets import DefsAssets as Defs
from quantkit.markets import *



def test_cells_known_markets(*active_cells, globals={}):
	''' objective:
	 	- primary: seemless 1-to-1 conversion to and from a jupyter notebook
	 	- primary: functional in raw python form (or ipython); independence from jupyter frameworks and tools
	 	- secondary: to have behavior similar to jupyter notebook
	 '''
	active_cells = set(active_cells)

	itn = IIncr()  # behaves like a mutable int and thus can increment

	if all([itn in active_cells, next(itn)]):
		# Testing Known Assets Pairs & Markets
		known_markets = KnownMarkets()
		known_pairs = KnownPairs()
		known_assets = KnownAssets()
		
		print(str(LessForm(known_assets)))
		print(str(LessForm(known_pairs)))
		print(str(LessForm(known_markets)))
	if all([itn in active_cells, next(itn)]):
		known_mkt = known_markets['btc',Defs.usds_by_vol,'binance']
		pp(known_mkt)
	if all([itn in active_cells, next(itn)]):
		iargs = [
			[Ind['btc',:,'binance']],
			[Ind['btc',Defs.usds_by_vol,'binance']],
			[Ind['btc',Defs.usds_by_vol,'coinbase-pro']],
			[Ind['btc',Defs.usds_set,'binance']],
			[Ind['btc',Defs.usds_set,'coinbase-pro']],  ]
		tr_form = TestRunsForm((known_markets.__getitem__,), iargs, items_fmt=pfmt)
		print(tr_form)
	
	if all([itn in active_cells, next(itn)]):
		# Testing next_clock_time
		from pyutils.time.timemetric import DefsTime, TM, next_clock_time, prev_clock_time
		print(prev_clock_time(TM.week, t_beginning='sunday')-TM.week)
		print(next_clock_time(TM.week, t_beginning='sunday')-TM.week)
	
	return locals()


# todo: relocate
def test_numiters():
	## test iternums next
	ns = iter(inum())
	print(next(ns), next(ns))
	ns = Ints()
	print(next(ns), next(ns))

	ns = IIncr()
	print(str(ns), str(next(ns)), str(next(ns)))
	
	## todo: test iternums iter
	assert ns == 2


if __name__=='__main__':
	from pymatics.store.trackrecords import time_run, DefsTrackRecord
	# test_numiters()
	# print(list(test_cells_known_markets(0,1)))
	output = time_run(test_cells_known_markets, 0, 1, 2)
	# output = test_cells_known_markets(0, 1)
	print(list(output))
	DefsTrackRecord.inst.save()

