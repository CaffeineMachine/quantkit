from pyutils.output.testscopes import *

# ictx = IterTestContexts(excludes=[2])
ictx = IterTestContexts(includes=[0])

if ictx('test prog'):
	from math import sin, pi
	from pyutils.utilities.iterutils import *
	from quantkit.output.paragraphic import *
	
	ofs_a, ofs_b = -6.5, 0.45
	square			= ([ofs_a]*9) + ([9+ofs_a]*9)
	sawtooth		= [y*2+ofs_a for y in range(9)]
	triangle		= sawtooth + sawtooth[::-1]
	sine			= [sin(ind*(2*pi/(9*4)))-ofs_b for ind in range(9*4)]
	iys_waveform	= zip(*[irepeat(waveform,'+') for waveform in [square,sawtooth,triangle,sine]])
	
	prog_bars		= ProgBars(width=[12,16,20,24], into=[[-9,9],[-9,9],[-9,9],[-1,1]], join=' ')
	single_line = False
	fmt_header = 'Waveforms:\n{:%s}|{:%s}|{:%s}|{:%s}' % (*prog_bars.width,)
	print(fmt_header.format(*Strs('square  saw  triangle  sine'.upper())))
	for t, ys_waveform in zip(range(9*4+1), iys_waveform):
		print(prog_bars.repr(ys_waveform))
		# print('%6s %6s %6s   %+.4f' % ys_waveform)
	
if ictx('test dots - bit access'):
	from time import sleep

	dots_ord = [Def.remap_bits(2**ind) for ind in range(8)]
	print('\n'.join([*map('{:08b}'.format, dots_ord)]), end='\n\n')
	print('{:08b}'.format(Def.remap_bits(0b10100000)))
	print('{:08b}'.format(Def.remap_bits(0b00000101)))
	
	print(repr(dots[0b00001011]), '{:08b}'.format(0b00001011))
	print(repr(dots[0b10110000]), '{:08b}'.format(0b10110000))
	print(repr(dots[0b00000111]), '{:08b}'.format(0b00000111))
	print(dots[0b000000111] + dots[0b00010000])

if ictx('test dots - bit access'):
	assert "⠁⡀" == dots[0b10000000] + dots[0b00010000]							# (l)eft (N)orth + (l)eft (S)outh
	assert "⠈⢀" == dots[0b00001000] + dots[0b00000001]							# (r)ight (N)orth + (r)ight (S)outh
	assert "⠇" == dots[0b11100000]
	assert "⠇" == dots[0b10000000 | 0b01000000 | 0b10100000]
	assert "⠀⣿" == dots[0b00000000] + dots[0b11111111]							# zero dots + all dots
	assert "⠁⠂⠄⡀⠈⠐⠠⢀" == ''.join((dots[2**i] for i in reversed(range(8))))	# all 8 bits in sequence

if ictx('test dots - test vertical scrolling graph'):
	single_line = False
	for t in range(200):
		print(single_line and '\b'*100 or '\n', end='', flush=True)
		print(scroll_plot([(sin(ind*pi/5.+ t*.05)+1.)*5 for ind in range(5)]), end='')
		sleep(.05)
	
if ictx('test dots'):
	print(scroll_plot([3., 3.1, 3.2, 3.5, 4, 6]))
	
	print(scroll_plot([n/100. for n in range(0,1000)]))
	print(scroll_plot([n/100. for n in range(0,1000)], 1000))
	sgl_anim = [	0b10000000, 0b01000000, 0b00100000, 0b00010000,
					0b00000001, 0b00000010, 0b00000100, 0b00001000, ]
	dbl_anim = [	0b11000000, 0b01100000, 0b00110000, 0b00010001,
					0b00000011, 0b00000110, 0b00001100, 0b10001000, ]
	for t in range(10):
		for sgl, dbl in zip(sgl_anim, dbl_anim):
			print('\b'*4+f'{dots[sgl]}  {dots[dbl]}', end='', flush=True)
			sleep(.4)
			print('\b'*4, end='', flush=True)