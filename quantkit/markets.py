from __future__ import annotations
import re
import os
import yaml
from collections import namedtuple
from pandas import DataFrame
from typing import Union as Or, Callable
from pathlib import Path

from pyutils.output.joins import *
from pyutils.structures.viewmaps import ViewMap, ViewMaps
from pyutils.patterns.idfactory import *
from pyutils.utilities.envs import *
from pyutils.utilities.iterutils import *
from pyutils.utilities.srcinfoutils import *

from quantkit.apidata_icls import *
from quantkit.query import *

from pymatics.store.trackrecords import time_run
pfileln_warn('from pymatics.store.trackrecords import time_run')

_log = None
# _log = print



class DefsMarkets:
	class state:
		t = T.min
		
	envs						= local_envs()
	false_values				= ['f', 'F', 'false', 'False']
	
	known_assets: KnownAssets	= None
	known_pairs: KnownPairs		= None
	known_markets: KnownMarkets	= None
	
	j_pairs						= Join('')
	jj_fields_yaml				= Joins('\n', ': ')
	jj_data_yaml				= Joins('_,\n_[\n_,\n]', '_,_[_]')
	jj_fields_csv				= Joins('_\n_\n_\n', '_,_- _')
	jj_items_csv				= Joins('_\n_\n_\n', '_,_- _')
	join_pairs, join_yaml_fields, join_yaml_data = j_pairs, jj_fields_yaml, jj_data_yaml  # todo: remove formerly named
	
	# local assets group defines
	usds_by_vol, usds_set = DefsAssets.usds_by_vol, DefsAssets.usds_set
Def = Defs = DefsMarkets


class YamlCoder:
	@classmethod
	def decode(cls, data): return data
	@classmethod
	def encode(cls, data): return data

	@classmethod
	def load(cls, path):
		with open(path, 'r') as file_stream:
			data = yaml.load(file_stream)
		result = cls.decode(data)
		return result
	@classmethod
	def dump(cls, path, data):
		data = cls.encode(data)
		with open(path, 'w') as file_stream:
			yaml.dump(file_stream)

def resolve_file_path(resource, file_ext=None, dir_path=None):
	# _log and _log('>>|   resolve_file_path()')
	file_name = str(resource)
	
	# resolve dir_path
	dir_path = dir_path or ('/' not in file_name and Defs.envs.paths.marketdata or '')  # todo: envs is not generalized config
	dir_path and not os.path.isdir(dir_path) and os.makedirs(dir_path)
	
	# resolve file_path
	file_name = file_name + (file_ext and not file_name.endswith('.'+file_ext) and '.'+file_ext or '')
	file_path = f'{dir_path}/{file_name}'
	file_path = re.sub(r'//+', '/', file_path)
	# _log and _log('  |<< resolve_file_path()')
	return file_path



class AssetItem(tuple):
	keys = ('symbol', 'name', 'is_fiat')

	symbol	= property(lambda self: self[0])
	name	= property(lambda self: self[1])
	is_fiat	= property(lambda self: self[2])
	kvs		= property(lambda self: (self[0], self[1:]))

	def __new__(cls, symbol, name='', is_fiat=False):
		return super().__new__(cls, (symbol, name, bool(is_fiat and is_fiat not in Def.false_values)))
	def coerce(obj):
		if isinstance(obj, AssetItem): return obj

		if isinstance(obj, (tuple, list)):
			result = AssetItem(*obj)
		elif isinstance(obj, dict):
			result = AssetItem(**obj)
		else:
			raise TypeError()

		return result
	def items(self): return zip(self.keys, self)
	
# class AssetMap(dict):
# 	''' Container for accessing known markets '''
# 	def __init__(self, data):
# 		_log and _log('>>|   AssetMap.__init__()')
# 		if isinstance(data, str):
# 			content = self.__class__.load(data, cast=False)
# 			data = content['data']
# 		if isinstance(data, list):
# 			data = ([symbol_name_is_fiat[0], symbol_name_is_fiat[-2:]] for symbol_name_is_fiat in data)
#
# 		dict.__init__(self, data)
# 		_log and _log('  |<< AssetMap.__init__()')
class AssetMap(dict):
	''' Container for accessing known markets '''
	i_kvs = property(lambda self: ((key, values) for key, *values in self._items))
	def __init__(self, data=()):
		_log and _log('>>|   AssetMap.__init__()')
		# if isinstance(data, str):
		# 	content = self.__class__.load(path=data, cast=False)
		# 	data = content['data']
		# if isinstance(data, list):
		# 	data = ([symbol_name_is_fiat[0], symbol_name_is_fiat[-2:]] for symbol_name_is_fiat in data)

		# dict.__init__(self, data)
		self._items = lseq(data, AssetItem)
		dict.__init__(self, self.i_kvs)
		_log and _log('  |<< AssetMap.__init__()')
	@staticmethod
	def coerce(obj):
		if isinstance(obj, MarketMap): return obj

		if isinstance(obj, list):
			result = MarketMap(obj)
		elif isinstance(obj, dict):
			result = MarketMap(obj['data'])
		else:
			raise TypeError()

		return result
	# @staticmethod
	# def load_csv(path:str, cast:Or[Callable, None]=...):
	# 	_log and pfuncqualln()
	# 	path = resolve_file_path(path)
	# 	with open(path, 'r') as istream:
	# 		data = yaml.load(istream)
	#
	# 	cast = cast is ... and AssetMap.coerce or cast
	# 	result = data if not cast else cast(data)
	# 	return result
	# def dump_csv(self, path=None):
	# 	path = resolve_file_path(path, 'csv')
	# 	with open(path, 'w') as ostream:
	# 		# write out header from buffer
	# 		fields = [
	# 			['columns', DefsJoins.j_values(AssetItem.keys)],
	# 			['data', ''],  ]
	# 		result = Defs.join_yaml_fields(fields)
	# 		ostream.write(result)
	#
	# 		# write out body from buffer
	# 		assets = ([symbol, name, int(is_fiat)] for symbol, (name, is_fiat) in self.items())
	# 		result = Defs.jj_items_yaml(assets)
	# 		ostream.write(result)
	# 	return path
	# load, dump = load_csv, dump_csv


PairItem = tuple

class PairMap(ViewMaps):
	''' Container for accessing known asset pairs '''
	def __init__(self, data):
		_log and pfuncqualln()
		if isinstance(data, str):
			content = self.__class__.load(path=data, cast=False)
			data = content['data']
		data = lseq(data, tuple)
		ViewMaps.__init__(self, to_keys=[Defs.join_pairs], items=data)
	@staticmethod
	def coerce(obj):
		if isinstance(obj, PairMap): return obj
		
		if isinstance(obj, list):
			result = PairMap(obj)
		elif isinstance(obj, dict):
			result = PairMap(obj['data'])
		else:
			raise TypeError()

		return result
	@staticmethod
	def load_yaml(path:str, cast:Or[Callable, None]=...):
		path = resolve_file_path(path)
		_log and _log(f'>>|   PairMap.load_yaml({path})')
		with open(path, 'r') as istream:
			data = yaml.safe_load(istream)
			
		cast = cast is ... and PairMap.coerce or cast
		result = data if not cast else cast(data)
		_log and _log(f'  |<< PairMap.load_yaml({path})')
		return result
	def dump_yaml(self, path=None):
		path = resolve_file_path(path, 'yaml')
		with open(path, 'w') as ostream:
			# write out header from buffer
			fields = [
				['columns', DefsJoins.jitems(['asset', 'base'])],
				['data', ''],  ]
			result = Defs.join_yaml_fields(fields)
			ostream.write(result)
			
			# write out body from buffer
			assets = ([symbol, name, int(is_fiat)] for symbol, (name, is_fiat) in self.items())
			result = Defs.join_yaml_data(assets)
			ostream.write(result)
		return path
	load, dump = load_yaml, dump_yaml



class MarketItem(tuple):
	keys = ('asset', 'base', 'exch_id', 'active')
	
	asset	= property(lambda self: self[0])
	denom	= property(lambda self: self[1])
	exch_id	= property(lambda self: self[2])
	active	= property(lambda self: self[3])
	pair	= property(lambda self: self[0] + self[1])
	
	base	= property(lambda self: self[1])  # todo: remove formerly named
	
	def __new__(cls, pair=None, exch_id=None, active=True, exchange=None, asset=None, base=None):
		denom = base
		if pair:
			try:
				result = Def.known_pairs[pair,0]	# get predetermined (asset, base) for ambiguous combined str
				asset, denom = result
			except Exception as e:
				asset, denom = pair, ''
				print(f'Unknown pair: {pair}')
		return super().__new__(cls, (asset, denom, exch_id, bool(active and active not in Def.false_values)))
	def coerce(obj):
		if isinstance(obj, MarketItem): return obj
		
		if isinstance(obj, (tuple, list)):
			result = MarketItem(*obj)
		elif isinstance(obj, dict):
			result = MarketItem(**obj)
		else:
			raise TypeError()
		
		return result
	def items(self): return zip(self.keys, self)
	
@declclass
class MarketMap(ViewMaps, ApiData):
	''' Container for accessing known markets '''
	name = 'markets'
	
	# def __init__(self, *pa, **ka): return time_run(self.init, *pa, **ka)
	# def init(self, data, query):
	def __init__(self, data, query):
		_log and _log('>>|   MarketMap.__init__()')
		if isinstance(data, str):
			content = self.__class__.load(path=data, cast=False)  # content = time_run(self.__class__.load, data, cast=False)
			data, query = content['data'], content['query']
		
		if isinstance(query, str):
			query = QueryStruct.from_signature(query)
		elif isinstance(query, dict):
			query = QueryStruct(**query)
		elif not isinstance(query, QueryStruct):
			raise ValueError(f'unhandled type for query: {query}')
		
		items = self.coerce_arg_items(data) 	# data = time_run(self.coerce_arg_items, data)
		ViewMaps.__init__(self, to_keys=3, items=items)
		ApiData.__init__(self, query)
		_log and _log('  |<< MarketMap.__init__()')
	@staticmethod
	def coerce(obj, columns=MarketItem.keys):
		if isinstance(obj, MarketMap): return obj
		
		if isinstance(obj, list):
			result = MarketMap(obj)
		elif isinstance(obj, dict):
			result = MarketMap(obj['data'], obj['query'])
		else:
			raise TypeError()

		return result
	@staticmethod
	def coerce_items(data):
		coerced = lseq(data, MarketItem.coerce)
		return coerced
	@staticmethod
	def coerce_arg_items(data):
		''' less generic but slightly faster; needed for unittests '''
		coerced = [MarketItem(*item) for item in data]
		return coerced
	@staticmethod
	def fast_load_yaml(s_data):
		# query: marketsall_any_any_1s-dur_1s-dt_010101-000000
		# columns: pair, exch_id, active
		# data: [
		data = {}
		lines = s_data.split('\n')
		for ind, line in enumerate(lines):
			if ': ' not in line:
				lines[:ind] = []
				break
			key, value = line.split(': ')
			data[key] = value
			if key == 'columns':
				data[key] = data[key].split(',')
			
		data['data'] = []
		lines[-1].startswith(']') and lines.pop()
		for line in lines:
			if line[0] == '[' and line[-2:] == '],':
				line = line[1:-2]
			item = line.split(',')
			item[2] = int(item[2])
			data['data'].append(item)
		return data
	def load_yaml(path:str, cast:Or[Callable, None]=...):
		_log and _log(f'Markets.load_yaml({path})')
		path = resolve_file_path(path)
		# with open(path, 'r') as istream:
		# 	# data = yaml.safe_load(istream)  # time_run(yaml.safe_load, istream)
		s_data = Path(path).read_text()
		data = MarketMap.fast_load_yaml(s_data)  # time_run(MarketMap.fast_load_yaml, s_data)
		
		cast = cast is ... and MarketMap.coerce or cast
		result = data if not cast else cast(data)
		return result
	def dump_yaml(self, path=None, query=None):
		query = query or self.query
		path = resolve_file_path(query, path, 'yaml')
		with open(path, 'w') as ostream:
			# write out header from buffer
			fields = [
				['query', str(query)],
				['columns', DefsJoins.j_values(MarketItem.keys)],
				['data', ''],  ]
			result = Defs.join_yaml_fields(fields)
			ostream.write(result)
			
			# write out body from buffer
			markets = ([pair, exch_id, int(active)] for pair, exch_id, active in self.data)
			result = Defs.join_yaml_data(markets)
			ostream.write(result)
		return path
	# member aliases
	load, dump = load_yaml, dump_yaml
MarketData = MarketMap


class KnownAssets(FactoryIds, AssetMap):
	default_path = 'known-assets.csv'
	_default_id = 'the default'
	def __init__(self, id=_default_id, path=None):
		super().__init__(id, path or self.default_path)
	def include(self, asset_keys):
		unknown_asset_keys = {asset_key for asset_key in asset_keys if asset_key not in self}
		asset_items =  {asset_key: AssetItem(asset_key) for asset_key in unknown_asset_keys}
		Def.known_assets.update(asset_items)
	def __getattr__(self, key):
		result = self.get(key) or super().__getattribute__(key)
		return result
		
class KnownPairs(FactoryIds, PairMap):
	default_path = 'pairs-known.yaml'
	_default_id = 'the default'
	def __init__(self, id=_default_id, path=None):
		super().__init__(id, path or self.default_path)
		
class KnownMarkets(FactoryIds, MarketMap):
	default_path  = 'markets-known.yaml'
	_default_id = 'the default'
	def __init__(self, id=_default_id, path=None):
		Def.known_assets = isnonecall(Def.known_assets, KnownAssets)
		Def.known_pairs = isnonecall(Def.known_pairs, KnownPairs)
		super().__init__(id, path or self.default_path, query=None)
		
		# syncronize known_markets with known_assets
		asset_keys = ijoin([[mkt_item.asset, mkt_item.denom] for mkt_item in self.data])
		Def.known_assets.include(asset_keys)
		a = 0

class KnownSymbols:
	def __getattr__(self, key):
		# asset_item = Def.known_assets[key]
		return key
		
Sym = KnownSymbols()
# Sym = Def.known_assets = KnownAssets()
# known_pairs: KnownPairs		= None
# known_markets: KnownMarkets	= None