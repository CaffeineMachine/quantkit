import ccxt
import re
import traceback as tb

from datetime import datetime
from dateutil import parser
from dataclasses import dataclass, field
from pprint import pprint as pp, pformat as pfmt
from functools import lru_cache

from pyutils.patterns.poly import *
from pyutils.extend.datastructures import *
from pyutils.structures.units import *
from pyutils.time import clock
from pyutils.structures.args import At
from pyutils.utilities.typeutils import *

from minimal import minpandas as pd

from quantkit.apidata_icls import *
from quantkit.assets import *

log = None
log = print


@lru_cache(maxsize=20)
def ccxt_cache(exch_id):
	exch_id = re.sub(r'[ -]', '', exch_id)
	return getattr(ccxt, exch_id)

class MetaQueryStruct:
	# static members
	_fmt_sig = 'needs override' # determines signature representation for struct parts
	_props = []					# used to construct items property
	
	def attrs(self, as_obj=Attrdict, subset=None, add_props=True):
		result = dict(self.__dict__)
		# result['signature'] = self.signature_repr(result)
		if issubclass(as_obj, MetaQueryStruct) or as_obj.__name__ in ('QueryResource', 'QueryTimeline'):
			subset = list(as_obj.__dataclass_fields__.keys())
		if subset:
			result = {k:v for k, v in result.items() if k in subset}
			add_props = False
		if add_props:
			result.update({prop: getattr(self, prop) for prop in self._props})
		# result.update({prop: getattr(self, prop) for prop in self._props})
		result = as_obj(**result) if as_obj else result
		return result
	def __hash__(self):			return hash(self.signature_repr(self))
	def __eq__(self, other):	return self.signature_repr(self) == other.signature_repr(other)
	def __str__(self):			return self.signature
	def __repr__(self):			return self.signature
	# def __repr__(self):			return f'QueryStruct({self.signature})'
	@classmethod
	def signature_repr(cls, query_items):
		query_items = 'Query' in query_items.__class__.__name__ and query_items.attr_dict or query_items
		sig_items = Defs.nil_query_fields.copy()
		sig_items.update(query_items)
		result = cls._fmt_sig.format(**sig_items)
		result = re.sub(r'None(-[^_]+_?)|/', '', result)
		return result
	@classmethod
	def from_signature(cls, signature):
		''' recreate a query from its string signature '''
		# given Query class _fmt_sig derive regex for query fields
		sig_regex = re.sub(r'(\{([a-z_]+)[^{}]*\})', r'(?P<\2>[a-zA-Z0-9-]+)', cls._fmt_sig)
		
		# given Query class regex search signature for field terms
		attrs = re.search(sig_regex, signature)
		# parse field strings
		query_kws = AttrDict(attrs.groupdict())
		if 'interval' in query_kws:
			query_kws.interval = DeltaTimeUnit(query_kws.interval)
		if 'duration' in query_kws:
			query_kws.duration = DeltaTimeUnit(query_kws.duration)
		if 'frame_start' in query_kws:
			query_kws.start = datetime.strptime(query_kws.pop('frame_start'), '%y%m%d-%H%M%S')
			
		query = cls(**query_kws)
		if 'frame_start' in query_kws:
			query.start = query_kws.frame_start
		return query
	
	# properties
	state = attr_dict = property(lambda self: self.attrs())
	signature = property(lambda self: self.signature_repr(self.attr_dict))
	
	
@dataclass
class QueryTimeFrame(MetaQueryStruct):
	# instance members
	duration: DeltaTimeUnit = field(default_factory=lambda: DeltaTimeUnit('1s'))
	interval: DeltaTimeUnit = field(default_factory=lambda: DeltaTimeUnit('1s'))
	
	# static members
	_fmt_sig = '{duration}-dur_{interval}-dt'
	_props = Strs('seconds size')
	__eq__, __hash__ = MetaQueryStruct.__eq__, MetaQueryStruct.__hash__
	
	def __init__(self, duration=None, interval=None):
		self.duration = DeltaTimeUnit(duration or interval or '1s')
		self.interval = DeltaTimeUnit(interval or duration or '1s')
		a = 0
		# super().__init__()
	@staticmethod
	def compute_size(duration, interval):
		# result = duration and int(duration.total_seconds() and duration.total_seconds() / interval.total_seconds()) or 0
		result = (interval and duration) and int(duration.total_seconds() / interval.total_seconds()) or 0
		return result

	# properties
	frame		= property(lambda self: (self.interval, self.duration))
	seconds		= property(lambda self: self.interval and self.interval.total_seconds())
	size		= property(lambda self: self.compute_size(self.duration, self.interval))
	td			= property(lambda self: self.duration)
	ti			= property(lambda self: self.interval)
	
@dataclass
class QueryTimeline(QueryTimeFrame):
	# instance members
	start: datetime = field(default=datetime.min)
	
	# static members
	_fmt_sig = f'{QueryTimeFrame._fmt_sig}_{{frame_start:%y%m%d-%H%M%S}}'
	_props = QueryTimeFrame._props + ['frame_start', 'frame_stop']
	__eq__, __hash__ = MetaQueryStruct.__eq__, MetaQueryStruct.__hash__
	__repr__ = MetaQueryStruct.__str__
	
	
	def __init__(self, duration=None, interval=None, start=None):
		if not isinstance(start, datetime) and isinstance(start, date):
			start = datetime.fromordinal(start.toordinal())
		self.start = start
		QueryTimeFrame.__init__(self, duration, interval)
	def __prep_args__(self, start=datetime.min, *args, signature=None, **kws):
		if not isinstance(start, datetime) and isinstance(start, date):
			start = datetime.fromordinal(start.toordinal())
		return args, dict(kws, start=start)
	def __lt__(self, other):
		result = self.start < other.start
		return result
	
	# properties
	frame_start	= property(lambda self: self.duration and clock.prev_tick(self.duration, self.start, 'sun'))
	frame_stop	= property(lambda self: self.duration and (self.frame_start + self.duration))
	
@dataclass
class QueryResource(MetaQueryStruct):
	# instance members
	data_name: str	= None
	exch_id: str	= None
	market: 'types.Any'	= None
	
	# static members
	_fmt_sig = '{data_name}_{exch_id}_{market}'
	_props = ['exchange', 'data_type']
	__eq__, __hash__ = MetaQueryStruct.__eq__, MetaQueryStruct.__hash__
	
	def __init__(self, data_name=None, exch_id=None, market=None):
		self.data_name = data_name
		self.exch_id = exch_id
		self.market = market
	
	# properties
	source		= property(lambda self: self.exch_id)
	resource	= property(lambda self: self.market)
	data_type	= property(lambda self: DefsApiData.get_data_type(self))
	exchange	= property(lambda self: self.exch_id if self.exch_id in ('any', None) else ccxt_cache(self.exch_id))


class QueryStruct(QueryResource, QueryTimeline):
	_fmt_sig = f'{QueryResource._fmt_sig}_{QueryTimeline._fmt_sig}'
	_props = QueryResource._props + QueryTimeline._props
	__eq__, __hash__ = MetaQueryStruct.__eq__, MetaQueryStruct.__hash__
	__repr__ = MetaQueryStruct.__str__
	
	def __init__(self, data_name=None, exch_id=None, market=None, duration=None, interval=None, start=None):
		QueryResource.__init__(self, data_name, exch_id, market)	# note: breakpoint here fails
		QueryTimeline.__init__(self, duration, interval, start)
		a = 0

from pyutils.patterns.capsules import VarsCapsule

class QueryPresets:
	def __init__(self, presets, logger=print):
		self.presets = set(callpas(VarsCapsule, presets, ka=dict(keys='ti td')))
		self.logger = logger
		ptrn = r'(?P<a>[^-_]*)-(?P<b>[^-_]*)_?'
		preset_fields = [[
			Vars(zip(Strs('value abrv'), field))
			for field in re.findall(ptrn, str(preset))]
			for preset in presets]
		preset_names = [
			'_'.join([f'{field.abrv}{field.value}' for field in fields])
			for fields in preset_fields]
		self.named_presets = dict(zip(preset_names, presets))
	def __iter__(self):				return iter(self.values)
	def __dir__(self):				return super().__dir__() + list(self.keys)
	def __getattr__(self, attr):	return self.named_presets[attr]
	def __str__(self):
		result = ''.join([f'\n\t{str(key)!r},  ' for key in self.keys])
		result = f'{self.__class__.__name__}([{result}])'
		return result
	def __repr__(self):
		result = ''.join([f'\n\t{repr(preset)},  ' for preset in self])
		result = f'{self.__class__.__name__}([{result}])'
		return result
	def handle_invalid(self, msg, error=False, valid=False):
		if valid: return
		
		assert not error, msg
		
		self.logger(msg)
	def verify(self, query, error=False):
		valid = query in self.presets
		self.handle_invalid(f'Query: {query} is not a valid preset signature.', error, valid)
		return valid
	def verify_resource(self, query, error=False):
		valid = query in self.presets
		self.handle_invalid(f'QueryResource: {query} is not a valid preset signature.', error, valid)
		return valid
	def verify_timeframe(self, query, error=False):
		valid = query in self.presets
		self.handle_invalid(f'QueryTimeframe: {query} is not a valid preset signature.', error, valid)
		return valid
	def verify_timeline(self, query, error=False):
		query = copy(query)
		query.__class__ = QueryTimeFrame
		valid = query in self.presets
		prev_clock = clock.prev_tick(query.duration, query.start, 'sun')	# get start of clock cycle w/ duration
		valid &= query.start == prev_clock 							# does query.start match start of clock cycle
		self.handle_invalid(f'QueryTimeline: {query} is not a valid preset signature.', error, valid)
		return valid
	
	# method aliases
	__contains__ = verify_timeline
	
	# properties
	keys		= property(lambda self: self.named_presets.keys())
	values		= property(lambda self: self.named_presets.values())
	items		= property(lambda self: self.named_presets.items())


class QValsInd:
	query = data_name, source, resource, duration, interval, start = tuple(range(6))
	qresource	= data_name, source, resource
	qtimeline	= duration, interval
	qtimeframe	= *qtimeline, start
	
	# all unique values
	sections = _value_superset = *zip(query), query, qresource, qtimeline, qtimeframe
	
	# aliases for abbrv. and legacy compatibility
	name, src, res, dur, dt, t = _values = query
	mkt = market = resource
	exch_id = source
	qres, qtl, qtf = qresource, qtimeline, qtimeframe
	
class QArgsInd:
	query = data_name, source, asset, base, duration, interval = tuple(range(6))
	market = At[asset:base]
	qresource = data_name, source, asset, base
	qtimeline = duration, interval
	
	# all unique values
	sections = _value_superset = *zip(query), query, qresource, qtimeline
	
	# aliases for abbrv. and legacy compatibility
	name, src, _, _, dur, dt = _values = query
	resource = mkt = market
	exch_id = source
	qres, qtl = qresource, qtimeline
QAI, QVI = QArgsInd, QValsInd

class QueryVals(QValsInd):
	format = '{}_{}_{}_{}-dur_{}-dt_{}'
	formats = format.split('_')
	ptrn_qsig = r'([^_]*)_([^_]*)_([^_]*)_([^_]*)-dur_([^_]*)-dt_?([^_]*)'
	sections = tseq(QValsInd.sections, '_'.join, formats.__getitem__)
	by_ind = dict(zip(QValsInd.sections, sections))
	
	@classmethod
	def to_vals(cls, query):
		qvals = query
		if typelike(query, MetaQueryStruct):
			qvals = query.data_name, query.source, query.resource, query.duration, query.interval, query.start
		elif typeof(query, str):
			qvals = tuple(re.match(cls.ptrn_qsig, query).groups())

		assert typeof(qvals[0], str), f'Failed conversion from query={query} to query_args={qvals}'
		return qvals

	@classmethod
	def to_sig(cls, qvals, inds=QValsInd.query):
		if typeof(qvals, str):	return qvals
		
		if typeof(qvals, dict):
			qvals = tmap(Defs.fields, qvals)
		elif typeof(qvals, MetaQueryStruct):
			qvals = tmap(Defs.fields, qvals, getter='__getattr__')
		
		qsig = cls.by_ind[inds].format(*qvals)
		return qsig
	
	# aliases
	fmt, fmts = format, formats
	coerce = to_vals
	
class QueryArgs(QArgsInd):
	format = '{}_{}_{}{}_{}-dur_{dt}-dt'  # dt will default to be duration
	formats = re.findall(r'\{[^\{]+', format)
	sections = tseq(QArgsInd.sections, [lambda fmt: fmt.strip('_'), ''.join], formats.__getitem__)
	by_ind = dict(zip(QArgsInd.sections, sections))

	@classmethod
	def from_vals(cls, qvals):
		qargs = qvals[:QVI.res+1] + ('',) + qvals[QVI.dur:QVI.dur+1]
		return qargs
	
	@classmethod
	def to_args(cls, query):
		qvals = QV.to_vals(query)
		qargs = cls.from_vals(qvals)
		return qargs

	@classmethod
	def to_sig(cls, qargs, inds=QArgsInd.query):
		if typeof(qargs, str):	return qargs
		
		if typeof(qargs, dict):
			qvals = tmap(Defs.fields, qargs)
		elif typeof(qargs, MetaQueryStruct):
			qvals = tmap(Defs.fields, qargs, getter='__getattr__')
		
		qsig = cls.by_ind[inds].format(*qvals)
		return qsig
	
	# aliases
	fmt, fmts = format, formats
	
QA, QV = QueryArgs, QueryVals
# QArgs, QVals = QueryArgs, QueryVals

# future task: QVals = AttrMap.from_namespace(QVI, to_value=lambda ind: '_'.join(formats[ind]))
# future task: QArgs = AttrMap.from_namespace(QAI, to_value=lambda ind: '_'.join(formats[ind]))


class DefsQuery:
	nil_query_fields = {field:'*' for field in list(QueryStruct.__dataclass_fields__) + QueryStruct._props}
	nil_query_fields.update(dict(start=datetime.min))
	
	fields = tstrs('data_name source resource duration interval start')
	
Defs = DefsQuery

	
Defaults = QueryPresets