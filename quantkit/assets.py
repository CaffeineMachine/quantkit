from dataclasses import dataclass, field
from pyutils.output.joins import DefsJoins
from typing import Union as Or

from pyutils.defs.typedefs import *
from pyutils.structures.maps import FullVars
from pyutils.structures.strs import *
from pyutils.patterns.accessors import Attr
# from pyutils.patterns.accessors import Accessor

__all__ = 'Symbol Symbols Market Sym Syms Mkt Symbols DefsAssets'.split()


# todo: replace abrv with name or asset
@dataclass(frozen=True)
class Asset:
	symb: str	= None
	name: str	= None
	
	def __str__(self): return self.symb
	def __iter__(self): return iter([self.symb, self.base])
	def __getattr__(self, base):
		result = self.__dict__.get(base) or self.__dict__.setdefault(base, _Market(self.symb, base))
		return result
	def exch_markets(self, exch=None, as_asset=True, as_base=False):
		# markets = Defs.market_cache[exch] + []
		result = [market for market in Defs.market_cache[exch]
			if (as_asset and self.symb == market.abrv) or (as_base and self.symb == market.base)]
		return result
	# method aliases
	# abrv = Attr.symb  # todo: remove formerly named
Symbol = Asset

@dataclass(frozen=True)
class _Market:
	''' Cryptocurrency symbols '''
	asset: str
	denom: str
	
	def __str__(self): return self.abrv+self.base
	def __iter__(self): yield from (self.abrv, self.base)
	# method aliases
	abrv, base = Attr.asset, Attr.denom  # todo: remove formerly named
	
class SymbolAttrFab:
	def __getattr__(self, abrv):
		result = self.__dict__.get(abrv) or self.__dict__.setdefault(abrv, Asset(abrv))
		return result
Sym = SymbolAttrFab()

class Market(_Market):
	btcusdt		= Sym.btc.usdt
	btcusd		= Sym.btc.usd
	xrpbtc		= Sym.xrp.btc
	xrpusdt		= Sym.xrp.usdt
	ethbtc		= Sym.eth.btc
	ethusdt		= Sym.eth.usdt
	ltcbtc		= Sym.ltc.btc
	ltcusdt		= Sym.ltc.usdt
	bchbtc		= Sym.bch.btc
	bchusdt		= Sym.bch.usdt
	eosbtc		= Sym.eos.btc
	eosusdt		= Sym.eos.usdt
Mkt = Market


class SymbolItems:
	# properties
	ranked = property(isntset)
	
	def __repr__(self):
		return f'Symbols({self})'
	@staticmethod
	def is_ranked(assets):
		return isntset(assets)
	# todo: is_ranked = staticmethod(isntset)  use this instead
	
class SymbolsSeq(SymbolItems, list):
	''
	def __str__(self): return DefsJoins.j_list(self)
class SymbolsSet(SymbolItems, set):
	''
	def __str__(self): return DefsJoins.j_set(self)

# todo: eliminate SymbolsItems, SymbolsSeq and SymbolsSet
# todo: demote to plain function or make list or set items a member
class Symbols(SymbolItems):
	''' A set of assets defined as either ranked or unranked (as in ordered by precedence) '''
	def __new__(cls, items:Or[list,set,str], ranked:bool=None):
		if ranked is None:
			ranked = SymbolItems.is_ranked(items)
		if isinstance(items, str):
			strs_ctor = ranked and LStrs or SStrs
			items = strs_ctor(items)
			
		syms_ctor = ranked and SymbolsSeq or SymbolsSet
		result = syms_ctor(items)
		return result
Syms = Symbols


class DefsAssets:
	usds_by_vol	= LStrs('usd usdt tusd usdc usds pax')
	usds_set	= KStrs('usd usdt tusd usdc usds pax')
	
	known_assets = {}
	
	market_cache = FullVars(
		fill=[Sym.btc.usd],
		binance=[Sym.btc.usdt],
	)
Defs = DefsAssets