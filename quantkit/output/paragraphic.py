import math

import itertools as it

import re

from functools import lru_cache
from dataclasses import dataclass, field

from msnctl.utility.qntutils import *
from pykit.patterns.members import *
from pyutils.structures.strs import *
from pyutils.utilities.iterutils import *
from pyutils.output.colorize import *
from pyutils.structures.maps import *
from pyutils.structures.args import *
from msnctl.utility.defs import The # todo: The must be relocated to pyutils

dataclassnrepr = dataclass(repr=False)


'''
dots is a lookup array length 256 (string rather) of all braille chars
dots is ordered such that its the bits of its index match the braille dot pattern
because it is clear who ever defined the utf8 encoding standard for
braille characters was not a concerned with computer science concepts

Examples:
assert "⠁⡀" == dots[0b10000000] + dots[0b00010000], 'oops' # (l)eft (N)orth + (l)eft (S)outh
assert "⠈⢀" == dots[0b00001000] + dots[0b00000001], 'oops' # (r)ight (N)orth + (r)ight (S)outh
assert "⠇" == dots[0b11100000], 'oops'
assert "⠇" == dots[0b10000000 | 0b01000000 | 0b10100000], 'oops'
assert "⠀⣿" == dots[0b00000000] + dots[0b11111111], 'oops' # zero dots + all dots
assert "⠁⠂⠄⡀⠈⠐⠠⢀" == ''.join((dots[2**i] for i in reversed(range(8)))), 'oops'  # all 8 bits in sequence
'''

# todo: RELO
def into_of_nums(nums, fab=None):
	into = min(nums), max(nums)
	into = into if not fab else fab(into)
	return into
def nn_of_nums(nums):  return into_of_nums(nums, fab=NN)


def _remap_bits(bits, map_from_l7r0=None):
	map_from_l7r0 = map_from_l7r0 or DefsParagraphic.l0r7_at_l7r0
	bits_remap = [map_from_l7r0[i] for i in range(8) if 2**i & bits]
	bits_sum = sum(bits_remap)
	return bits_sum
def _get_colr(color, layer='fore', shade='dark'):
	if callable(color): return color
	
	str_color_set = (layer == 'back' and Back or Fore).__dict__.__getitem__
	str_color = str_color_set(color)
	return str_color
class DefsParagraphic:
	#region quad defines
	quads_zorder_l3r0		= u' ▘▝▀▖▌▞▛▗▚▐▜▄▙▟█'
	#endregion
	
	#region prog defines
	into_progs				= NN(0.,1.)
	progs_l0r8				= u' ▏▎▍▌▋▊▉█'
	progs_n0s8				= u' ▁▂▃▄▅▆▇█'
	progsx = progs			= progs_l0r8
	progsy					= progs_n0s8
	prog_full = prog_1		= progs[-1]
	prog_none = prog_0		= progs[0]
	#endregion
	
	#region dot defines
	dots_l7r0 = '''
	⠀ ⠁ ⠂ ⠃ ⠄ ⠅ ⠆ ⠇ ⡀ ⡁ ⡂ ⡃ ⡄ ⡅ ⡆ ⡇
	⠈ ⠉ ⠊ ⠋ ⠌ ⠍ ⠎ ⠏ ⡈ ⡉ ⡊ ⡋ ⡌ ⡍ ⡎ ⡏
	⠐ ⠑ ⠒ ⠓ ⠔ ⠕ ⠖ ⠗ ⡐ ⡑ ⡒ ⡓ ⡔ ⡕ ⡖ ⡗
	⠘ ⠙ ⠚ ⠛ ⠜ ⠝ ⠞ ⠟ ⡘ ⡙ ⡚ ⡛ ⡜ ⡝ ⡞ ⡟
	⠠ ⠡ ⠢ ⠣ ⠤ ⠥ ⠦ ⠧ ⡠ ⡡ ⡢ ⡣ ⡤ ⡥ ⡦ ⡧
	⠨ ⠩ ⠪ ⠫ ⠬ ⠭ ⠮ ⠯ ⡨ ⡩ ⡪ ⡫ ⡬ ⡭ ⡮ ⡯
	⠰ ⠱ ⠲ ⠳ ⠴ ⠵ ⠶ ⠷ ⡰ ⡱ ⡲ ⡳ ⡴ ⡵ ⡶ ⡷
	⠸ ⠹ ⠺ ⠻ ⠼ ⠽ ⠾ ⠿ ⡸ ⡹ ⡺ ⡻ ⡼ ⡽ ⡾ ⡿
	⢀ ⢁ ⢂ ⢃ ⢄ ⢅ ⢆ ⢇ ⣀ ⣁ ⣂ ⣃ ⣄ ⣅ ⣆ ⣇
	⢈ ⢉ ⢊ ⢋ ⢌ ⢍ ⢎ ⢏ ⣈ ⣉ ⣊ ⣋ ⣌ ⣍ ⣎ ⣏
	⢐ ⢑ ⢒ ⢓ ⢔ ⢕ ⢖ ⢗ ⣐ ⣑ ⣒ ⣓ ⣔ ⣕ ⣖ ⣗
	⢘ ⢙ ⢚ ⢛ ⢜ ⢝ ⢞ ⢟ ⣘ ⣙ ⣚ ⣛ ⣜ ⣝ ⣞ ⣟
	⢠ ⢡ ⢢ ⢣ ⢤ ⢥ ⢦ ⢧ ⣠ ⣡ ⣢ ⣣ ⣤ ⣥ ⣦ ⣧
	⢨ ⢩ ⢪ ⢫ ⢬ ⢭ ⢮ ⢯ ⣨ ⣩ ⣪ ⣫ ⣬ ⣭ ⣮ ⣯
	⢰ ⢱ ⢲ ⢳ ⢴ ⢵ ⢶ ⢷ ⣰ ⣱ ⣲ ⣳ ⣴ ⣵ ⣶ ⣷
	⢸ ⢹ ⢺ ⢻ ⢼ ⢽ ⢾ ⢿ ⣸ ⣹ ⣺ ⣻ ⣼ ⣽ ⣾ ⣿
	'''.replace(' ', '').replace('\n', '').replace('\t', '')
	l0r7_at_l7r0	= tuple((2**i for i in reversed(range(8))))
	_dots_l0r7_ord	= [_remap_bits(ind, mapped) for ind, mapped in zip(range(256), irepeat(l0r7_at_l7r0))]
	dots_l0r7		= ''.join([d_l7r0[ind] for ind, d_l7r0 in zip(_dots_l0r7_ord, irepeat(dots_l7r0))])
	#endregion
	
	#region other defines
	mask_xs_prio			= 0b1000, 0b1000, 0b0011, 0b1100, 0b1010, *([0b1111]*4)
	mask_xs_uniform			= (0b1111,)*8
	mask_xs_ys_uniform		= [[0b1111],[0b1111]]
	mask4_xs_ys_uniform		= [[0b1111],[0b1000,0b0100,0b0010,0b0001]]
	class Ptrn:
		_preset = [  # todo: illustrative to see most common mask definitions; but its unusable as is
			'⠀', 0b00000000, '⡇', 0b11110000, '⢸', 0b00001111, '⣿', 0b11111111,
			'⢠', 0b00000011, '⠘', 0b00001100, '⡄', 0b00110000, '⠃', 0b11000000,
			'⣼', 0b00111111, '⢻', 0b11001111, '⣧', 0b11110011, '⡟', 0b11111100,
			'⣤', 0b00110011, '⢣', 0b11000011, '⡜', 0b00111100, '⠛', 0b11001100,
		]
		_named0 = left, right, north, south = l, r, n, s = (
			0b11110000, 0b00001111, 0b11001100, 0b00110011, )
		_named1 = a, b, c, d, e, f, g, h = (
			0b10000000, 0b01000000, 0b00100000, 0b00010000, 0b00010000, 0b00000100, 0b00000010, 0b00000001, )
	
	s_colrs = str_color		= tstrs('bluel magental redl yellowl greenl cyanl white whitel')
	# colrs = str_colors		= [*map(_get_colr.__get__(object), str_color)]
	colrs = str_colors		= [*map(_get_colr, str_color)]
	#endregion
Def = DefsParafx = DefsParagraphic
dots, progs = Def.dots_l0r7, Def.progs


#region scrolling plots (as in *v*ertical)
Def.inds_xs_rscl	= _at_xrescale, _at_ind, _at_scolr, _at_nummask = range(4)
Def.inds_chars		= _at_x, _at_ndot, _at_style1, _at_style2 = range(4)
Def.dot_count_horz	= 2
Def.dot_count_vert	= 4


def shift_by(vals, basis=None):
	basis = basis if basis is not None else vals[-1]
	vals = [val - basis for val in vals]
	return vals

_at_x, _at_y, _at_last		= 0, 1, -1
def plot_row(*vals_pa, into=(0.,10), size=60, masks=(), colrs=Def.s_colrs, get_colr=_get_colr, **ka):
	''' produce row str with each value in nums translated to its dot char
		:param size: required output str length
		:param masks: mask paired with each value in nums sequence; as in zip(nums, masks)
	'''
	# resolve inputs;  ... => xrscls_colr = [[x_rescale, _, colr, mask]
	into = istype(into, NN) and into or NN(into)
	scale = size * Def.dot_count_horz * .99 / (into.high - into.low)
	vals_row_len = len(vals_pa[0])
	xs = [(val - into.low) * scale for val in ijoin(vals_pa)]					# rescale x *into* size bounds here
	assert 0. <= min(xs) <= max(xs) <= size*Def.dot_count_horz	# early sanity check: in bounds?
	
	# _masks = list(mask_xs_ys if len(vals_pa) < 2 else it.product(*mask_xs_ys))
	# _masks = [mask_x & mask_y for mask_x, mask_y in it.product(*mask_xs_ys)]
	# _masks = irepeat([irepeat([mask_x & mask_y for mask_x in masks[_at_x]], '+') for mask_y in masks[_at_y]], '+')
	_masks = ljoin([lrepeat([mask_x & mask_y for mask_x in masks[_at_x]], '+%s'%vals_row_len) for mask_y in masks[_at_y]])
	
	# colrs, masks = irepeat(colrs, '+'), irepeat(list(masks if len(vals_pa) < 2 else it.product(*masks)), '+')
	colrs, masks, masks_in = irepeat(colrs[:vals_row_len], '+'), _masks, masks
	xrscls_colr = sorted([(x, ind, *pa) for ind, x, *pa in iindexj(zip(xs, colrs, masks))])  # sort by x
	
	# get composite dot char at each x;  rscls_scolr => chars = [[dot_x, dots_key, fore_colr, back_colr], ...]
	chars, ptrn_frame = [], 0
	n_dot_old, x_old = 0, -1
	for x_rscl, _, colr, num_mask in xrscls_colr:
		x, is_right_dot = int(x_rscl // Def.dot_count_horz), int(x_rscl % Def.dot_count_horz)
		num_mask_frame = num_mask >> ptrn_frame & 0xf
		n_dot = (is_right_dot and Def.Ptrn.right or Def.Ptrn.left) & (num_mask_frame | (num_mask_frame<<4))
		
		if x == x_old:
			chars[_at_last][_at_ndot] |= n_dot
		else:
			chars.append([x, n_dot, get_colr(colr), asis])  # todo: move get_colr out of loop
		x_old, n_dot_old = x, n_dot
	# add filler to then end of chars to match *length* if needed;  chars => chars
	if chars[_at_last][_at_x] != size-1:
		chars += [[size-1, 0, asis, asis]]
	# todo: chars[-1][_at_x] != length-1 and chars.append([length-1, 0, asis, asis])
		
	# resolve output as line of chars at respective x with spaces between:  chars => text
	# todo: texts, x_old = ['']*len(chars), -1
	texts, x_old = [], -1
	for x, dot_mask, style1, style2 in chars:
		dx = int(x - x_old - 1)
		# print(x, n_dot, dx)
		texts += [' '*dx, style2(style1(dots[dot_mask]))]
		x_old = x
	text = ''.join(texts)
	return text
scroll_plot = plot_row

# def dotmask_of_nums(nums=None, num_masks=(), into=(0.,10.), length=60, s_colrs=Def.s_colrs, get_colr=_get_colr, **ka):
# 	# get composite dot char at each x;  rscls_scolr => chars = [[dot_x, dots_key, fore_colr, back_colr], ...]
# 	chars, ptrn_frame = [], 0
# 	n_dot_old, x_old = 0, -1
# 	for x_rescale, _, s_colr, num_mask in rscls_scolr:
# 		x, right_dot = int(x_rescale // 2), int(x_rescale % 2)
#
# 		num_mask_frame = num_mask >> ptrn_frame & 0xf
# 		n_dot = (right_dot and Def.Ptrn.right or Def.Ptrn.left) & (num_mask_frame | (num_mask_frame<<4))
# 		if x == x_old:
# 			chars[-1][_at_ndot] |= n_dot
# 		else:
# 			chars.append([x, n_dot, get_colr(s_colr), asis])
# 		x_old, n_dot_old = x, n_dot
# 	# add filler to then end of chars to match *length* if needed;  chars => chars
# 	if chars[-1][_at_x] != length-1:
# 		chars += [[length-1, 0, asis, asis]]
# 	return chars
#
# def plot_row_sub(nums=None, num_masks=(), low=0., high=10., length=60, s_colors=Def.str_color, get_colr=_get_colorrepr, **ka):
# 	# resolve output:  ... => text
# 	texts, x_old = [], -1
# 	for x, n_dot, style1, style2 in chars:
# 		dx = int(x - x_old - 1)
# 		# print(x, n_dot, dx)
# 		texts += [' '*dx, style2(style1(dots[n_dot]))]
# 		x_old = x
# 	pass


@dataclassnrepr
class Ranging:
	names = fixed, snap, interp = Strs('fixed snap interp')
	hint_low: float		= None
	hint_high: float	= None
	space_low: float	= 4.
	space_high: float	= 4.
	mid: float			= None
	mode: str			= snap
	snap_amt: float		= 2**.5
	interp_amt: float	= .1
	#region class members
	_default_space		= .2
	zoom_in_rate		= 80
	zoom_out_rate		= None
	zoom_in_delay		= 0
	zoom_out_delay		= 0
	resized				= ''
	# properties
	hint				= property(lambda self: (self.hint_low, self.hint_high))
	space				= property(lambda self: (self.space_low, self.space_high))
	#endregion
	def __repr__(self):
		result = '({:8.2f},{:8.2f}){:2} '.format(self.hint_low, self.hint_high, self.resized)
		return result
	@staticmethod
	def center_values(*values, mid=None):
		if mid is None: return values
		
		low_val, high_val = min(values), max(values)
		d_mid = max(abs(low_val-mid), abs(high_val-mid))
		
		values  = mid - d_mid, mid + d_mid
		return values
	@staticmethod
	def space_values(*values, space_low=None, space_high=None):
		low_val, high_val = min(values), max(values)
		space_low, space_high = space_low or 0., space_high or 0.
		values  = low_val - space_low, high_val + space_high
		return values
	def adjust(self, values, hint_low=None, hint_high=None, space_low=None, space_high=None, mid=None, can_zoom_in=None, **ka):
		mode = self.mode
		ranging_method = self.method[mode]
		can_zoom_in = isnoneput(can_zoom_in, self.zoom_in_delay == 0)

		values = self.center_values(*values, mid=isnoneput(mid, self.mid))
		space = dict(space_low=isnoneput(space_low, self.space_low), space_high=isnoneput(space_high, self.space_high))
		values = self.space_values(*values, **space)
		
		hint = dict(hint_low=isnoneput(hint_low, self.hint_low), hint_high=isnoneput(hint_high, self.hint_high))
		(self.hint_low, self.hint_high), zoom = ranging_method(*values, **hint, can_zoom_in=can_zoom_in, **ka)
		self.zoom_in_delay = ''.join(zoom) != '  ' and self.zoom_in_rate or atleast(0, self.zoom_in_delay-1)
		
		self.resized = ''.join(zoom)
		return self.hint
	@staticmethod
	def adjust_fixed(low_hint=None, high_hint=None, *values, space_low=None, space_high=None, **ka):
		low, high = ...,...
		return low, high
	@staticmethod
	def adjust_snap(*values, hint_low=None, hint_high=None, snap_amt=snap_amt, can_zoom_in=True, can_drift=False, perc=.2, **ka):
		val = nn_of_nums(values)
		hint = NN(isnoneput(hint_low, val.low), isnoneput(hint_high, val.high))
		d_hints = hint.high-hint.low
		less = NN(hint.high-(d_hints/snap_amt), hint.low+(d_hints/snap_amt))
		more = NN(min(values[0], hint.high-(d_hints*snap_amt)), max(values[1], hint.low+(d_hints*snap_amt)))
		
		calc, zoom = NN(hint), NN(*'  ')
		if can_drift:
			if less.low < val.low and can_zoom_in:
				calc.low, zoom.low = less.low, '-'
			elif val.low < hint.low:
				calc.low, zoom.low = more.low, '+'
			else:
				calc.low = hint.low
			
			if val.high < less.high and can_zoom_in:
				calc.high, zoom.high = less.high, '-'
			elif hint.high < val.high:
				calc.high, more.high = zoom.high, '+'
			else:
				calc.high = hint.high
		else:
			if less.low < val.low and val.high < less.high and can_zoom_in:
				calc, zoom = NN.ramp(hint, less, factor=perc), NN(*'--')
			elif val.low < hint.low or hint.high < val.high:
				calc, zoom = more, NN(*'++')
			else:
				calc = hint
			
		return calc, zoom
	@staticmethod
	def adjust_interp(low_hint=None, high_hint=None, *values, space_low=None, space_high=None, **ka):
		low, high = ..., ...
		return low, high
	# todo: centering function can be reduced and put in terms of low & high values
	
	method = Vars()
	# method aliases
	__call__ = adjust
Ranging.method = AttrMap(zip(Ranging.names, [Ranging.adjust_fixed, Ranging.adjust_snap, Ranging.adjust_interp]))

@dataclass
class ScrollPlotter:
	length: int			= 60
	mid: float			= None
	colrs: int			= Def.s_colrs
	get_scl: int		= _get_colr
	mode: object		= Ranging.snap  # can give str or ranging function
	ranging: object		= None  # can give str or ranging function
	masks:list			= None
	def __init_subclass__(cls):
		cls.__call__ = cls.plot
	def __post_init__(self):
		self.ranging = Ranging(mid=self.mid or 0.)  # todo: remove 'or 0' when you have cleaner method of ranging config
		self.masks = self.masks or Def.mask_xs_prio
	def plot(self, *vals, length=None, colrs=None, get_scl=None, **ka):
		# resolve inputs
		vals = lcallpas(shift_by, vals)
		size, colrs, get_colr = length or self.length, colrs or self.colrs, get_scl or self.get_scl
		hint_lows, hint_highs = lzip(*icallpas(into_of_nums, vals, **ka))				# todo: delegate to Ranging
		hint = self.low_hint, self.high_hint = min(hint_lows), max(hint_highs)		# todo: delegate to Ranging
		hint = self.ranging.adjust(hint, **ka)
		
		# resolve output
		result = plot_row(*vals, into=hint, size=size, colrs=colrs, get_colr=get_colr, masks=self.masks, **ka)
		result += self.ranging.resized != '  ' and str(self.ranging) or ''
		return result
	__call__ = plot
GraphV = ScrollGraph = PlotrV = ScrollPlotter

# @dataclass
# class ScrollPlotterQuad(ScrollPlotter):
# 	vals_buffer_capacity = 4
# 	def __post_init__(self):
# 		self.masks = self.masks or Def.mask4_xs_ys_uniform
# 		super().__post_init__()
# 		self.vals_buffer = [None] * self.vals_buffer_capacity
# 		self.vals_buffer_count = 0
# 	def plot(self, vals, **ka):
# 		# resolve inputs
# 		self.vals_buffer[self.vals_buffer_count] = list(vals)
# 		self.vals_buffer_count += 1
# 		if self.vals_buffer_count != self.vals_buffer_capacity:
# 			return
#
# 		# resolve output
# 		result = super().plot(*self.vals_buffer, **ka)
# 		self.vals_buffer_count = 0
# 		return result
# GraphV4 = ScrollGraphQuad = PlotrV4 = ScrollPlotterQuad
#endregion

#region sliding plots (as in *h*orizontal)
# todo
#endregion

#region quant utils todo: RELO qntutils
def mod1(flt:float) ->float:
	''' get float point remainder (value right of decimal point) of given *flt* '''
	mod_1 = flt - int(flt)
	return mod_1
def unitize(flt:float) ->[int,float]:
	''' split by decimal point into whole value and float point remainder sub-components on given *flt* '''
	whole = int(flt)
	mod_1 = flt - whole
	return whole, mod_1
truncmod1 = unitize
#endregion

#region colorrepr prog poles
@dataclass
class ColorReprPole:
	reprcolor: callable		= None
	back: callable			= None
	fore: callable			= None
	posv: bool				= True
	def __post_init__(self):
		if self.reprcolor:
			assert not isiter(self.reprcolor) or self.back is None
			self.fore, self.back = self.reprcolor if isiter(self.reprcolor) else (self.reprcolor, self.back)
	def repr(self, progs, posv=True):
		s_progs = self.fore(progs) if self.fore else progs
		s_progs = self.back(s_progs) if self.back else s_progs
		s_progs = flip(s_progs) if self.posv != posv else s_progs
		return s_progs
	__call__ = repr
	
@dataclass
class ColorReprPoles:
	reprcolor: callable		= None
	low: callable			= None
	high: callable			= None
	neut: callable			= field(default=gray)
	prime					= property(lambda self: self.high)
	polar					= property(lambda self: self.low)
	def __post_init__(self):
		if self.reprcolor:
			assert not isiter(self.reprcolor) or self.low is None
			self.high, self.low = self.reprcolor if isiter(self.reprcolor) else (self.reprcolor, self.low)
		
		self.high = isiter(self.high) and ColrPole(self.high) or self.high
		self.low = isiter(self.low) and ColrPole(self.low, posv=False) or self.low
	def repr(self, progs, posv=True, swap=False):
		if posv == neutral: return gray(grayb(progs))
		repr_pole = self.polar if swap else self.prime
		s_progs = repr_pole(progs) if repr_pole else progs
		s_progs = flip(s_progs) if posv == negv else s_progs
		return s_progs
	@classmethod
	def of(cls, obj):
		if not obj or typeof(obj, cls): return obj
		
		obj = cls(obj)
		return obj
	__call__ = repr
ColrPole, ColrPoles =  ColorReprPole, ColorReprPoles

Def.colr_poles = ColorReprPoles([[greenl, cyanb],[redl, magentab]])
#endregion

#region progs
@lru_cache(maxsize=70)
def _nnsizes_of_sizing(width, into_low, into_high):
	'''
		## Naming convention for this algo
		ratios: posv width & negv width; high/width,low/width; must sums to given width
		sz|size (int): the number of contiguous chars of prog_full '█' or prog_none ' '
		Example:
		width=3, into=[-((4)),5] => proprx:.555, ratios:(1.3333),1.6666 => ratios_sz:(1),2; scale_to_fit:(1)/((4)).
		width=4, into=[-4,((5))] => proprx:.555, ratios:1.7777,(2.2222) => ratios_sz:2,(2); scale_to_fit:(2)/((5)).
	'''
	# width,into => ratios_sz,scale_to_fit
	abs_sz = NN(abs(into_low), abs(into_high))
	abs_sum = abs_sz.high + abs_sz.low
	
	ratios =  [abs_sz.low*width/abs_sum, abs_sz.high*width/abs_sum]
	ratios = NN(*ratios[::abs_sz.high >= abs_sz.low and 1 or -1])
	ratio_floor,ratio_ceil,step = (ratios.high % 1. <= .5) and (ratios.high,ratios.low,-1) or (ratios.low,ratios.high,1)  # which ratio is closest to whole
	ratios_sz = NN(*[int(ratio_floor), int(ratio_ceil+.9999)][::step])
	return ratios_sz, abs_sz
def nnsizes_of_sizing(width:int=1, into=Def.into_progs):	return _nnsizes_of_sizing(width, into.low, into.high)

def repr_progs(value, width:int=1, into=Def.into_progs, colr_poles=None):
	'''
		## Naming convention for this algo
		ratios: posv width & negv width; high/width,low/width; must sums to given width
		sz|size (int): the number of contiguous chars of prog_full '█' or prog_none ' '
		Example:
		width=3, into=[-((4)),5] => proprx:.555, ratios:(1.3333),1.6666 => ratios_sz:(1),2; scale_to_fit:(1)/((4)).
		width=4, into=[-4,((5))] => proprx:.555, ratios:1.7777,(2.2222) => ratios_sz:2,(2); scale_to_fit:(2)/((5)).
	'''
	# resolve inputs
	# into = (typeof(into, NN) and into) or (isiter(into) and NN(*into)) or (typeof(into, (int,float)) and NN(0,into)) or throw()
	into = (typeof(into, NN) and into) or NN(typeof(into,(int,float)) and [0,into] or isiter(into) and into or throw())
	colr_poles = istype(colr_poles,ColrPoles) and colr_poles or colr_poles and ColrPoles(colr_poles) or Def.colr_poles
	
	# width,into => ratios_sz,scale_to_fit
	ratios_sz, abs_sz = nnsizes_of_sizing(width, into)
	scale_to_fit = min(keep([abs_sz.low and ratios_sz.low/abs_sz.low, abs_sz.high and ratios_sz.high/abs_sz.high]))
	
	# interpolate value from *into* defined range to given *width* range
	value_fit = scale_to_fit * into.into(value) - .00001
	
	# is_posv => fmt_progs4
	posv = is_posv(value_fit)
	pole = pole_of(value)
	swap = not posv
	prog_polar, fmt_progs4, value_l2r = (
		(' ', '{3}{0}{1}{2}', value_fit) if posv else
		('█', '{0}{1}{2}{3}', ratios_sz.low+value_fit))
	prog_l, prog_r = '█', ' '

	# get sizes of contiguous prog parts consisting of either prog_full '█' or prog_none ' '
	value_whole,value_mod1 = unitize(value_l2r)
	sz_l, sz_r, sz_polar = value_whole, ratios_sz.high-value_whole-1, ratios_sz.low
	
	# create progs: (l)eft, (r)ight, polar from corresponding sz|sizes
	progs_l, progs_r, progs_polar = sz_l*prog_l, sz_r*prog_r, prog_polar*sz_polar
	progs_polar = sz_polar and colr_poles.repr(progs_polar,pole, swap) or ''
	prog_join = progs[int(value_mod1 * 8.)]
	
	# use formatters to combine prog parts: (l)eft, join, (r)ight, polar
	s_prog = fmt_progs4.format(progs_l, prog_join, progs_r, progs_polar)
	s_prog = colr_poles.repr(s_prog, pole, swap)
	return s_prog

@dataclassnrepr
class ProgBars:
	values: [float]			= None
	widths: int				= None
	intos: int				= None
	colr_poles: ColrPoles	= Def.colr_poles
	horz: bool				= True
	join: bool				= ' '
	__repr__				= MbrRepr('widths  intos')
	def __post_init__(self):
		self.join = isstras(self.join, fab=lambda s: s.join)
		assert self.widths and self.intos
	def repr(self, values):
		self.values = values
		widths, colr_poles = isiteras(self.widths, orfab=irepeat), isiteras(self.colr_poles, orfab=irepeat)
		
		# self.ls_bars = lcallargs(repr_progs, values, width=width, into=self.into, colr_poles=colr_poles)
		self.ls_bars = lcallkas(repr_progs, value=values, width=widths, into=self.intos, colr_poles=colr_poles)
		self.s_bars = self.join(self.ls_bars)
		return self.s_bars
	def header(self):
		result = ...
		return result
	__call__ = repr
@dataclass
class ProgBars4:
	def repr(self, values):
		result = super().repr(values)
		return result
	__call__ = repr
#endregion


@dataclass
class ValueQueue:
	call: callable
	vals_queue_capacity		= 4
	def __post_init__(self):
		# self.masks = self.masks or Def.mask4_xs_ys_uniform
		super().__post_init__()
		self.vals_queue = [None] * self.vals_queue_capacity
		self.vals_queued = 0
	def on_values(self, vals, **ka):
		# resolve inputs
		self.vals_queue[self.vals_queue_capacity] = list(vals)
		self.vals_queued += 1
		if self.vals_queued != self.vals_queue_capacity: return
		
		# resolve output
		result = call(*self.vals_queue, **ka)
		self.vals_queued = 0
		return result
	__call__ = on_values
