from pathlib import Path
from dataclasses import dataclass, field

from pyutils.utilities.iterutils import lre
from pyutils.extend.datastructures import Attrdict
from pyutils.structures.strs import Strs
from pyutils.patterns.decor import declclass


@dataclass
@declclass
class ApiDataABC:
	query: 'types.Any' = None
	
	# static defines
	name = 'apidata'
	extensions = Strs('csv json yaml yml')
	file_ptrn_fmt = r'/({names})[^_]*(_[^_]+){{5}}\.({extensions})'
	file_ptrn = ''
	query_expiration = None
	
	@classmethod
	def __post_decl__(cls):  # todo: test __subclass_init__
		cls.file_ptrn = cls.file_ptrn_fmtr()
		cls.file_ptrn = cls.file_ptrn
	@classmethod
	def file_ptrn_fmtr(cls, names=None, extensions=None):
		names = [cls.name] if names is None else []
		extensions = cls.extensions if extensions is None else extensions
		result = cls.file_ptrn_fmt.format(names='|'.join(names), extensions='|'.join(extensions))
		return result
	@classmethod
	def filter_files(cls, data_files):
		''' identify the subset of data_files matching file pattern for this ApiData '''
		api_data_files = lre(cls.file_ptrn, data_files)
		return api_data_files
	@classmethod
	def scan(cls, dir_path=None):
		''' scan dir_path for files matching the pattern for this apidata type '''
		data_files = list(Path(dir_path).iterdir())
		api_data_files = cls.filter_files(data_files)
		return api_data_files
	@staticmethod
	def is_complete(query, data): return True
	def __getstate__(self):	raise NotImplemented()
	@staticmethod
	def load(path):			raise NotImplemented()
	def dump(self, path):	raise NotImplemented()
ApiData = ApiDataABC


class DefsApiData:
	# data_types = [Bars, DepthBook, Trades, Tickers, Markets]
	data_types = []
	# _data_name_types = Attrdict()
	aliases = Attrdict()

	@classmethod
	def update_data_types(cls, add=(), drop=(), clear=False):
		if clear: cls.data_types = set()
		
		cls.data_types = [*{*cls.data_types} - {*drop} | {*add}]
		cls._data_name_types = Attrdict({dt.name :dt for dt in cls.data_types})
	@classmethod
	def get_data_type(cls, data_name):
		data_name = hasattr(data_name, 'data_name') and data_name.data_name or data_name
		data_name = cls.aliases.get(data_name, data_name)
		return cls._data_name_types.get(data_name, None)
	@classmethod
	def get_data_name(cls, data_type):
		return data_type.name
DefsApiData.update_data_types()

Defs = DefsApiData
