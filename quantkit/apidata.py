from dataclasses import dataclass, field
from sortedcontainers import SortedDict
from pyutils.extend.datastructures import *
from minimal import minpandas as pd
from quantkit.apidata_icls import *
from quantkit.markets import MarketData


__all__ = Strs('DefsApiData ApiData Bars Trades DepthBook MarketData Tickers')

class Bars: pass
class Trades: pass
class DepthBook: pass


	
@dataclass
@declclass
class Bars(ApiData):
	# instance members
	content: 'types.Any' = None
	
	# static defines
	name = 'ohlc'
	_columns = Strs('timestamp open high low close unit_volume volume')
	
	@staticmethod
	def from_csv(path, query):
		data = pd.read_csv(f'{path}/{query.signature}.csv')
		data = Bars(data, query)
		return data
	@classmethod
	def to_csv(cls, path, data, query):
		pd.DataFrame(data, columns=cls._columns).to_csv(f'{path}/{query.signature}.csv', index=False)
	@staticmethod
	def is_complete(query, data):
		return query.size <= len(data)
	# def to_csv(self, path):
	# 	pd.DataFrame(self.content, columns=self._columns).to_csv(f'{path}/{self.query.signature}.csv', index=False)
	# def is_complete(self):
	# 	return self.query.size <= len(self.content)
	
	# method aliases
	load, dump = from_csv, to_csv
OHLC = Bars


@dataclass
@declclass
class DepthBook(ApiData):
	asks: SortedDict = None
	bids: SortedDict = None
	
	# static defines
	name = 'depth'
	
	@staticmethod
	def from_json(path, query): raise NotImplemented()
	def to_json(self, path): raise NotImplemented()
	
	# method aliases
	load, dump = from_json, to_json
	
	
@dataclass
@declclass
class Trades(ApiData):
	name = 'trades'
@dataclass
@declclass
class Tickers(ApiData):
	name = 'tickers'


# DefsApiData.data_types = [Bars, DepthBook, Trades, Tickers, MarketData]
DefsApiData.update_data_types([Bars, DepthBook, Trades, Tickers, MarketData])
DefsApiData.aliases.bars = 'ohlc'
# DefsApiData.known_markets = PairsKnown
# DefsApiData.known_markets =




# @dataclass
# class PriceBars:
# 	# instance members
# 	content: 'types.Any'
# 	query_items: AttrDict = field(default_factory=AttrDict)
# 	columns: list = list
#
# 	def to_csv(self, path):
# 		pd.DataFrame(self.content, columns=self.columns).to_csv(f'{path}/{self.query_items.signature}.csv', index=False)
# 	@staticmethod
# 	def from_csv(query_items, path):
# 		data = pd.read_csv(f'{path}/{query_items.signature}.csv')
# 		data = PriceBars(data, query_items)
# 		return data
# 	def is_complete(self):
# 		return self.query_items.size <= len(self.content)
