from setuptools import setup

setup(name='quantkit',
      version='0.0.1',
      description='Python structures for market data',
      author='Lee Hailey',
      author_email='slhaile01@gmail.com',
      license='MIT',
      packages=['quantkit'],
      zip_safe=False)