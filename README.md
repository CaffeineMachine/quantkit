# QuantKit

## Creators

**Stanley Hailey**

- https://gitlab.com/CaffeineMachine
- <http://caffeine-machine.com/>

## License
Released for free under the MIT license, which means you can use it for almost
any purpose (including commercial projects). Credit is appreciate but not required.